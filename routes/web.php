<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', function (){
	return 'Hello!';
});

Route::get('welcome/{ten?}', function($ten){
	return 'Xin chao '. $ten;
}) ->where('ten','[A-Za-z]+');

Route::group(['prefix'=>'admin'], function(){
	Route::get('hello', function(){return "Hello Admin";});
	Route::get('bye', function(){return 'Bye Admin';});
});

// vd goi view
Route::get('hello/{ten}', function($ten){
	return view('hello', ['user'=> $ten]);
});
